# rest-read-json-v2




## Getting started



Hi and welcome, this repository contains the source code for reading a json file from php language. Data is consumed
by using the curl utility from php. This code reads the json response from a REST API interface, process it and show it
on a HTML table.

Available online at (soon)


Code is organized as follows:
.

1. A php file name  "JsonTable" which use the curl, json_encode functions for consuming and processing a json file exposed as web  
   service response. It also uses foreach operator for processing the entities from the json file. A html table has been used for displaying the info from the json file.

         
